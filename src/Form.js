import React, { useState } from "react";
import { useForm } from "react-hook-form";
import "./Form.css";
import axios from 'axios';

const CNPJForm = () => {

  const { register, handleSubmit, errors } = useForm();

  const [nome, setNome] = useState('')

  function onSubmit(data) {
    console.log("Data submitted: ", data);
    axios.post('http://localhost:8080/create', data).then(response => {
        alert('Cadastros Efetuado com sucesso!');
        alert(response.data);
    });
  }

  function handleChange(e) {
    console.log(e.target.value);
    var cnpj = e.target.value;
    axios.get('http://localhost:8080/findcnpj/'+cnpj)
    .then(response => {
        console.log("Data submitted: ", response);
        //[this.nome] = response.data.CNPJForm.nome;
    }).catch((erro) => {
        axios.get('https://www.receitaws.com.br/v1/cnpj/'+cnpj)
        .then(response => {
            console.log("Data submitted: ", response);
            const { nome, value } = e.target
            this.setState({ [nome]: value })
            //[this.nome] = response.data.CNPJForm.nome;
        }).catch(error => {
            console.error(error);
          });
    });
  }

return (
    <div className="anderson-form">
        <fieldset>
            <legend>
                <h2>Dados de Cadastro</h2>
            </legend>

            <form onSubmit={handleSubmit(onSubmit)} noValidate>
                <label htmlFor="cnpj">CNPJ</label>
                <input
                type="text"
                id="cnpj"
                name="cnpj"
                ref={register({ required: "CNPJ inválido!" })}
                onBlur={handleChange}
                />
                {errors.cnpj && <p className="error">{errors.cnpj.message}</p>}

                <label htmlFor="nome">Razão Social</label>
                <input
                type="text"
                id="nome"
                name="nome"
                value={nome}
                ref={register({ required: "Razão Social inválido!" })}
                />
                {errors.nome && <p className="error">{errors.nome.message}</p>}

                <label htmlFor="cep">CEP</label>
                <input
                type="text"
                id="cep"
                name="cep"
                mask="99999-999"
                slotChar="_____-___"
                ref={register({ required: "CEP inválido!" })}
                />
                {errors.cep && <p className="error">{errors.cep.message}</p>}

                <label htmlFor="logradouro">Logradouro (Endereço)</label>
                <input
                type="text"
                id="logradouro"
                name="logradouro"
                ref={register({ required: "Endereço inválido!" })}
                />
                {errors.logradouro && <p className="error">{errors.logradouro.message}</p>}

                <label htmlFor="numero">Número</label>
                <input
                type="text"
                id="numero"
                name="numero"
                ref={register({ required: "Número inválido!" })}
                />
                {errors.numero && <p className="error">{errors.numero.message}</p>}

                <label htmlFor="complemento">Complemento</label>
                <input
                type="text"
                id="complemento"
                name="complemento"
                ref={register({ required: "Complemento inválido!" })}
                />
                {errors.complemento && <p className="error">{errors.complemento.message}</p>}

                <label htmlFor="municipio">Município</label>
                <input
                type="text"
                id="municipio"
                name="municipio"
                ref={register({ required: "Cidade inválida!" })}
                />
                {errors.municipio && <p className="error">{errors.municipio.message}</p>}

                <label htmlFor="uf">UF</label>
                <select name="uf" id="uf" 
                ref={register({ required: "Selecione um estado" })}
                >
                    <option value="">Selecione um Estado</option> 
                    <option value="AC">Acre</option> 
                    <option value="AL">Alagoas</option> 
                    <option value="AM">Amazonas</option> 
                    <option value="AP">Amapá</option> 
                    <option value="BA">Bahia</option> 
                    <option value="CE">Ceará</option> 
                    <option value="DF">Distrito Federal</option> 
                    <option value="ES">Espírito Santo</option> 
                    <option value="GO">Goiás</option> 
                    <option value="MA">Maranhão</option> 
                    <option value="MT">Mato Grosso</option> 
                    <option value="MS">Mato Grosso do Sul</option> 
                    <option value="MG">Minas Gerais</option> 
                    <option value="PA">Pará</option> 
                    <option value="PB">Paraíba</option> 
                    <option value="PR">Paraná</option> 
                    <option value="PE">Pernambuco</option> 
                    <option value="PI">Piauí</option> 
                    <option value="RJ">Rio de Janeiro</option> 
                    <option value="RN">Rio Grande do Norte</option> 
                    <option value="RO">Rondônia</option> 
                    <option value="RS">Rio Grande do Sul</option> 
                    <option value="RR">Roraima</option> 
                    <option value="SC">Santa Catarina</option> 
                    <option value="SE">Sergipe</option> 
                    <option value="SP" >São Paulo</option> 
                    <option value="TO">Tocantins</option>
                </select>

                {errors.uf && <p className="error">{errors.uf.message}</p>}

                <label htmlFor="telefone">Telefone</label>
                <input
                type="text"
                id="telefone"
                name="telefone"
                ref={register({ required: "Telefone inválido!" })}
                />
                {errors.telefone && <p className="error">{errors.telefone.message}</p>}

                <label htmlFor="email">E-mail</label>
                <input
                type="email"
                id="email"
                name="email"
                ref={register({
                    required: "Enter your e-mail",
                    pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                    message: "Entre com um e-mail válido!",
                    },
                })}
                />
                {errors.email && <p className="error">{errors.email.message}</p>}

                <button type="submit">CADASTRAR</button>

            </form>

        </fieldset>

    </div>
  );
};

export default CNPJForm;