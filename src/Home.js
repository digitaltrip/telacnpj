import React from 'react';
import logo from './logo.svg';
import {Link} from 'react-router-dom';
 
function Home(){
    return (
      <div className="App">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <Link to="/cadastro">Acessar cadastro</Link>
        </p>
      </div>
    );
}
 
export default Home;